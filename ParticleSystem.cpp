#include "ParticleSystem.h"

// Constructor that just calls the init function
Particle::Particle(Pos StartPos, GLfloat TerminalVelocity, Pos WindVelocity, GLfloat Gravity, GLfloat EyeX, GLfloat EyeZ, GLfloat BgColourR, GLfloat BgColourG, GLfloat BgColourB) {
    init(StartPos, TerminalVelocity, WindVelocity, Gravity, EyeX, EyeZ, BgColourR, BgColourG, BgColourB);
}

// Initialises the particle based on the parameters
void Particle::init(Pos StartPos, GLfloat TerminalVelocity, Pos WindVelocity, GLfloat Gravity, GLfloat EyeX, GLfloat EyeZ, GLfloat BgColourR, GLfloat BgColourG, GLfloat BgColourB) {
    
    pos = StartPos;
    terminalVelocity = TerminalVelocity;
    velocity.x = WindVelocity.x;
    velocity.y = 0.0;
    velocity.z = WindVelocity.z;
    acceleration = Pos(0.0, Gravity, 0.0);

    // Calculate the distance from the eye in order to work out the colour
    GLfloat distanceFromEyePercentage = distance(pos, Pos(EyeX, 0.0, EyeZ)) / 70;
    if (distanceFromEyePercentage < 0.2)
        distanceFromEyePercentage = 0.2;
    else if (distanceFromEyePercentage > 0.8)
        distanceFromEyePercentage = 0.8;
    colourR = BgColourR - BgColourR * (1 - distanceFromEyePercentage);
    colourG = BgColourG - BgColourG * (1 - distanceFromEyePercentage);
    colourB = BgColourB - BgColourB * (1 - distanceFromEyePercentage);

    rippleCount = 20;
    rippleRadius = 0.0;
    rippleRadiusVelocity = 0.03;

    state = RAIN;
}

// Update particle
void Particle::update() {

    // update velocity with acceleration
    velocity.x += acceleration.x;
    if (velocity.y + acceleration.y > terminalVelocity) {
        velocity.y += acceleration.y;
    }
    velocity.z += acceleration.z;

    // Update position with velocity
    pos.x += velocity.x;
    pos.y += velocity.y;
    pos.z += velocity.z;

    // If the particle has hit the ground, it is now a ripple
    if (pos.y <= 0) {
        pos.y = 0;
        state = RIPPLE;
        colourR = 0.06;
        colourG = 0.3;
        colourB = 0.37;
        velocity = Pos(0.0, 0.0, 0.0);
    }
    // If the particle is already a ripple
    if (state == RIPPLE) {
        rippleCount--; // Reduce the count
        // If it is dead
        if (rippleCount == 0) {
            state = DEAD;
            return;
        }
        // Update radious and colour
        rippleRadius += rippleRadiusVelocity;
        colourR += (0.1 - 0.06) / rippleCount;
        colourG += (0.39 - 0.3) / rippleCount;
        colourB += (0.59 - 0.37) / rippleCount;
    }
}

// For calculating the distance between 2 points
GLfloat Particle::distance(Pos pos1, Pos pos2) {
    return sqrt(pow(pos2.x - pos1.x, 2) + pow(pos2.z - pos1.z, 2));
}

// Initialises the particle system
void ParticleSystem::init(GLint NumParticles, GLfloat BgR, GLfloat BgG, GLfloat BgB) {
    
    numParticles = NumParticles;
    bgR = BgR;
    bgG = BgG;
    bgB = BgB;

    maxDistanceFromEye = 50.0;
    skyY = 100.0;
    terminalVelocity = -1.5;
    gravity = -0.2;
    windVelocity = Pos(0.0, 0.0, 0.0);
    rainLength = 2.0;
    renderingSystem = 0;

    // Calculate the direction of the rain based on the wind direction and the rainlength
    GLfloat velocityMagnitude = sqrt(pow(windVelocity.x, 2) + pow(terminalVelocity, 2) + pow(windVelocity.z, 2));
    Pos velocityUnitVector = Pos(0, 0, 0);
    if (velocityMagnitude != 0) {
        velocityUnitVector.x = windVelocity.x / velocityMagnitude;
        velocityUnitVector.y = terminalVelocity / velocityMagnitude;
        velocityUnitVector.z = windVelocity.z / velocityMagnitude;
    }
    else {
        velocityUnitVector.y = 1;
    }
    rainDirection.x = velocityUnitVector.x * rainLength;
    rainDirection.y = velocityUnitVector.y * rainLength;
    rainDirection.z = velocityUnitVector.z * rainLength;

    // Create all the particles
    for (int i = 0; i < numParticles; i++) {
        GLfloat randVelocity = getRand((-terminalVelocity - 0.5) * 10, (-terminalVelocity + 0.5) * 10) / 10.0; // Random velocity between 1.5 and 2.5
        Particle p(getStartPosition(), -(randVelocity), windVelocity, gravity, eyePointX, eyePointZ, bgR, bgG, bgB);
        particles.push_back(p);
    }

    isPaused = false;

}

// Update the particle system
void ParticleSystem::update(GLfloat EyePointX, GLfloat EyePointZ) {

    // Check if it's paused
    if (!isPaused) {
        // Update eye point
        eyePointX = EyePointX;
        eyePointZ = EyePointZ;
        // Update all the particles
        for (int i = 0; i < numParticles; i++) {
            particles[i].update();
            // If a particle is dead, recycle it
            if (particles[i].getState() == DEAD) {
                recycleParticle(i);
            }
        }
    }
    // Needs redisplaying
    glutPostRedisplay();
}

// Render the particle system
void ParticleSystem::render() {
    glDisable(GL_LIGHTING);

    Pos pos;

    if (renderingSystem == 0) {

        // Render rain
        glDepthRange(0.1, 1.0);
        glBegin(GL_LINES); // Lines
        for (int i = 0; i < numParticles; i++) {
            // Check that it's a raindrop
            if (particles[i].getState() == RAIN) {
                pos = particles[i].getPos();
                glColor3f(particles[i].getColourR(), particles[i].getColourG(), particles[i].getColourB());
                glVertex3f(pos.x, pos.y, pos.z);
                glVertex3f(pos.x + rainDirection.x, pos.y + rainDirection.y, pos.z + rainDirection.z);
            }
        }
        glEnd();

        // Render ripples
        for (int i = 0; i < numParticles; i++) {
            // Check that it's a ripple
            if (particles[i].getState() == RIPPLE) {
                pos = particles[i].getPos();
                glDepthRange(0.1, 1.0);
                glBegin(GL_LINE_LOOP); // Circle
                glColor3f(particles[i].getColourR(), particles[i].getColourG(), particles[i].getColourB());
                renderCircle(pos, particles[i].getRippleRadius());
                glEnd();
            }
        }
    }
    
    // ----------------------------------------------------------------------------------------------

    else {

        for (int i = 0; i < numParticles; i++) {
            // Check that it's a raindrop
            if (particles[i].getState() == RAIN) {
                glDepthRange(0.1, 1.0);
                glBegin(GL_LINES);
                pos = particles[i].getPos();
                glColor3f(particles[i].getColourR(), particles[i].getColourG(), particles[i].getColourB());
                glVertex3f(pos.x, pos.y, pos.z);
                glVertex3f(pos.x + rainDirection.x, pos.y + rainDirection.y, pos.z + rainDirection.z);
                glEnd();
            }
            // Check if it's a ripple
            if (particles[i].getState() == RIPPLE) {
                pos = particles[i].getPos();
                glDepthRange(0.1, 1.0);
                glBegin(GL_LINE_LOOP); // Circle
                glColor3f(particles[i].getColourR(), particles[i].getColourG(), particles[i].getColourB());
                renderCircle(pos, particles[i].getRippleRadius());
                glEnd();
            }
        }
    }

}

// For rendering cicrles (ripples)
void ParticleSystem::renderCircle(Pos centre, GLfloat radius) {
    
    GLint numSegments = 16; // 16 lines in the circle
    GLfloat theta = (360.0 / numSegments) * (PI / 180.0);
    GLfloat currentAngle = 0.0;
    GLfloat x = 0.0; GLfloat z = 0.0;

    // Put a vertex at 16 points in the shape of a circle
    for (GLint i = 0; i < numSegments; i++) {
        currentAngle = theta * i;
        x = radius * cosf(currentAngle);
        z = radius * sinf(currentAngle);

        glVertex3f(x + centre.x, centre.y, z + centre.z);
    }
}

// Recycle a dead particle as a new one
void ParticleSystem::recycleParticle(GLint index) {
    GLfloat randVelocity = getRand((-terminalVelocity - 0.5) * 10, (-terminalVelocity + 0.5) * 10) / 10.0; // Random velocity between 1.5 and 2.5
    particles[index].init(getStartPosition(), -(randVelocity), windVelocity, gravity, eyePointX, eyePointZ, bgR, bgG, bgB);
}

// Get a random position on the sky plane
Pos ParticleSystem::getStartPosition() {
    Pos p = Pos(0.0, 0.0, 0.0);
    p.x = getRand(0, maxDistanceFromEye * 2) - maxDistanceFromEye + eyePointX;
    p.y = skyY;
    p.z = getRand(0, maxDistanceFromEye * 2) - maxDistanceFromEye + eyePointZ;
    return p;
}

// Get a random number between Min and Max
// Integers only
GLint ParticleSystem::getRand(GLint Min, GLint Max) {
    GLint difference = Max - Min;

    return (rand() % (difference + 1)) + Min;
}

// Set a new wind velocity
void ParticleSystem::setWindVelocity(Pos WindVelocity) {
    
    // Set the windVelocity variable and for every particle
    windVelocity = WindVelocity;
    for (unsigned int i = 0; i < particles.size(); i++) {
        particles[i].setWindVelocity(WindVelocity);
    }

    // Find a new rainDirection
    GLfloat velocityMagnitude = sqrt(pow(windVelocity.x, 2) + pow(terminalVelocity, 2) + pow(windVelocity.z, 2));
    Pos velocityUnitVector = Pos(0, 0, 0);
    if (velocityMagnitude != 0) {
        velocityUnitVector.x = windVelocity.x / velocityMagnitude;
        velocityUnitVector.y = terminalVelocity / velocityMagnitude;
        velocityUnitVector.z = windVelocity.z / velocityMagnitude;
    }
    else {
        velocityUnitVector.y = 1;
    }
    rainDirection.x = velocityUnitVector.x * rainLength;
    rainDirection.y = velocityUnitVector.y * rainLength;
    rainDirection.z = velocityUnitVector.z * rainLength;
}

// Set a new rain length
void ParticleSystem::setRainLength(GLfloat RainLength) {
    rainLength = RainLength;

    // Find a new rainDirection
    GLfloat velocityMagnitude = sqrt(pow(windVelocity.x, 2) + pow(terminalVelocity, 2) + pow(windVelocity.z, 2));
    Pos velocityUnitVector = Pos(0, 0, 0);
    if (velocityMagnitude != 0) {
        velocityUnitVector.x = windVelocity.x / velocityMagnitude;
        velocityUnitVector.y = terminalVelocity / velocityMagnitude;
        velocityUnitVector.z = windVelocity.z / velocityMagnitude;
    }
    else {
        velocityUnitVector.y = 1;
    }
    rainDirection.x = velocityUnitVector.x * rainLength;
    rainDirection.y = velocityUnitVector.y * rainLength;
    rainDirection.z = velocityUnitVector.z * rainLength;
}

// Set a new number of particles
void ParticleSystem::setNumParticles(GLint NumParticles) {
    
    numParticles = NumParticles;

    // If num particles is less than already in the vector, do nothing
    if (numParticles < particles.size())
        return;
    // If we need to create new particles
    else if (numParticles > particles.size()) {
        int particlesToAdd = numParticles - particles.size(); // Num of particles to add
        // Create the new particles
        for (int i = 0; i < particlesToAdd; i++) {
            GLfloat randVelocity = getRand((-terminalVelocity - 0.5) * 10, (-terminalVelocity + 0.5) * 10) / 10.0; // Random velocity between 1.5 and 2.5
            Particle p(getStartPosition(), -(randVelocity), windVelocity, gravity, eyePointX, eyePointZ, bgR, bgG, bgB);
            particles.push_back(p);
        }
    }
}

// Set a new gravity for all particles
void ParticleSystem::setGravity(GLfloat Gravity) {
    gravity = Gravity;

    for (unsigned int i = 0; i < particles.size(); i++) {
        particles[i].setGravity(gravity);
    }
}

// Set the average terminal velocity
void ParticleSystem::setTerminalVelocity(GLfloat TerminalVelocity) {
    terminalVelocity = TerminalVelocity;
    GLfloat randVelocity;
    for (unsigned int i = 0; i < particles.size(); i++) {
        // Get a new random velocity for each particle
        randVelocity = getRand((-terminalVelocity - 0.5) * 10, (-terminalVelocity + 0.5) * 10) / 10.0;
        particles[i].setTerminalVelocity(-randVelocity);
    }

    // Find a new rainDirection
    GLfloat velocityMagnitude = sqrt(pow(windVelocity.x, 2) + pow(terminalVelocity, 2) + pow(windVelocity.z, 2));
    Pos velocityUnitVector = Pos(0, 0, 0);
    if (velocityMagnitude != 0) {
        velocityUnitVector.x = windVelocity.x / velocityMagnitude;
        velocityUnitVector.y = terminalVelocity / velocityMagnitude;
        velocityUnitVector.z = windVelocity.z / velocityMagnitude;
    }
    else {
        velocityUnitVector.y = 1;
    }
    rainDirection.x = velocityUnitVector.x * rainLength;
    rainDirection.y = velocityUnitVector.y * rainLength;
    rainDirection.z = velocityUnitVector.z * rainLength;
}