#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H
#include <GL/freeglut.h>
#include <vector>
#define PI 3.141592654

// Pos Struct containing x, y, z floats
struct Pos {
public:
    Pos(GLfloat X = 0.0 , GLfloat Y = 0.0, GLfloat Z = 0.0) {
        x = X; y = Y; z = Z;
    }
    GLfloat x, y, z;
};

// Enum of the states which a particle can be in
enum ParticleState {
    RAIN = 0,
    RIPPLE = 1,
    DEAD = 2
};

// Particle class
class Particle {
private:
    Pos pos;
    GLfloat rainLength;
    GLfloat terminalVelocity;
    Pos rainDirection;
    Pos velocity;
    Pos acceleration;
    GLfloat colourR, colourG, colourB;
    ParticleState state;
    GLint rippleCount;
    GLfloat rippleRadius;
    GLfloat rippleRadiusVelocity;
public:
    Particle(Pos StartPos, GLfloat TerminalVelocity, Pos WindVelocity, GLfloat Gravity, GLfloat EyeX, GLfloat EyeZ, GLfloat BgColourR, GLfloat BgColourG, GLfloat BgColourB);
    void init(Pos StartPos, GLfloat TerminalVelocity, Pos WindVelocity, GLfloat Gravity, GLfloat EyeX, GLfloat EyeZ, GLfloat BgColourR, GLfloat BgColourG, GLfloat BgColourB);
    void update();
public:
    // Get and set functions
    Pos getPos() { return pos; }
    GLfloat getColourR() { return colourR; }
    GLfloat getColourG() { return colourG; }
    GLfloat getColourB() { return colourB; }
    ParticleState getState() { return state; }
    GLfloat getRippleRadius() { return rippleRadius; }
    void setWindVelocity(Pos WindVelocity) { velocity.x = WindVelocity.x; velocity.z = WindVelocity.z; }
    void setGravity(GLfloat Gravity) { velocity.y = Gravity; }
    void setTerminalVelocity(GLfloat TerminalVelocity) { terminalVelocity = TerminalVelocity; }
private:
    GLfloat distance(Pos pos1, Pos pos2);
};

// ParticleSystem class
class ParticleSystem {
private:
    std::vector<Particle> particles;
    GLint numParticles;
    GLfloat eyePointX, eyePointZ;
    GLfloat bgR, bgG, bgB;
    GLfloat skyY;
    GLfloat maxDistanceFromEye;
    GLfloat terminalVelocity;
    GLfloat gravity;
    Pos windVelocity;
    GLfloat rainLength;
    Pos rainDirection;
    bool isPaused;
    bool renderingSystem;
public:
    ParticleSystem() {}
    void init(GLint NumParticles, GLfloat BgR, GLfloat BgG, GLfloat BgB);
    void update(GLfloat EyePointX, GLfloat EyePointZ);
    void render();
private:
    void renderCircle(Pos centre, GLfloat radius);
    void recycleParticle(GLint index);
    Pos getStartPosition();
    GLint getRand(GLint Min, GLint Max);
public:
    GLint getNumParticles() { return numParticles; }
    void setNumParticles(GLint NumParticles);
    GLfloat getSkyY() { return skyY; }
    void setSkyY(GLfloat SkyY) { skyY = SkyY; }
    Pos getWindVelocity() { return windVelocity; }
    void setWindVelocity(Pos WindVelocity);
    GLfloat getRainLength() { return rainLength; }
    void setRainLength(GLfloat RainLength);
    GLfloat getGravity() { return gravity; }
    void setGravity(GLfloat Gravity);
    GLfloat getTerminalVelocity() { return terminalVelocity; }
    void setTerminalVelocity(GLfloat TerminalVelocity);
    void togglePause() { isPaused = !isPaused; }
    void toggleRenderingSystem() { renderingSystem = !renderingSystem; }
};


#endif