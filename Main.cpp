// Rain particle system for COMP37111 Mini Project
// This program uses some code from COMP27112 ex1

#include <GL/freeglut.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "ParticleSystem.h"

#define DEG_TO_RAD 0.017453293
#define PI 3.141592654

// Rendering constants
GLfloat white_light[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat light_position0[] = { -5.0, 7.0, -5.0, 0.0 };
GLfloat matSpecular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat matShininess[] = { 50.0 };
GLfloat matSurface[] = { 0.8, 0.5, 0.2, 0.1 };
GLfloat matEmissive[] = { 0.0, 1.0, 0.0, 0.1 };

// Dimensions of the window
GLint width = 1000;
GLint height = 600;

GLdouble lat, lon;                  // View angles (degrees)
GLdouble mlat, mlon;                // Mouse look offset angles
GLfloat  eyex, eyey, eyez;          // Eye point
GLfloat  centerx, centery, centerz; // Look point
GLfloat  upx, upy, upz;             // View up vector

// Particle System object
ParticleSystem* ps = new ParticleSystem();

// Calculates the look point of the camera and the view direction
void calculate_lookpoint(void) {

    // Combine the lat and mouse lat
    GLfloat latitudeCombined = lat + mlat;
    // Check that the latitude stays between 90.0 and -90.0
    if (latitudeCombined > 90.0)
        latitudeCombined = 90.0;
    else if (latitudeCombined < -90.0)
        latitudeCombined = -90.0;
    // Combine the lon and mouse lon
    GLfloat longitudeCombined = lon + mlon;

    // Calculate the viewDirection
    GLfloat viewDirectionX = cos(latitudeCombined * DEG_TO_RAD) * sin(longitudeCombined * DEG_TO_RAD);
    GLfloat viewDirectionY = sin(latitudeCombined * DEG_TO_RAD);
    GLfloat viewDirectionZ = cos(latitudeCombined * DEG_TO_RAD) * cos(longitudeCombined * DEG_TO_RAD);

    // Calculate the look at point (center)
    centerx = eyex + viewDirectionX;
    centery = eyey + viewDirectionY;
    centerz = eyez + viewDirectionZ;

} 

// Update function that calls the particle system update function
void update(int value) {

    ps->update(eyex, eyez);

    // Call this function again in 1/30th of a second
    glutTimerFunc(1000 / 30, update, 0);
}

// Displays everything on the screen
void display(void) {
    
    // Get ready to display
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, matShininess);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, matSurface);
    glLoadIdentity();
    calculate_lookpoint(); /* Compute the centre of interest   */
    gluLookAt(eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
    
    // Floor plane
    glDisable(GL_LIGHTING);
    glDepthRange(0.1, 1.0);
    glBegin(GL_QUADS);
    glColor3f(0.1, 0.39, 0.51);
    glVertex3f(-100, 0, -100);
    glVertex3f(100, 0, -100);
    glVertex3f(100, 0, 100);
    glVertex3f(-100, 0, 100);
    glEnd();
    
    
    // Direction lines
    glDepthRange(0.1, 1.0);
    glBegin(GL_LINES);
    // Red is x
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(centerx, centery, centerz);
    glVertex3f(centerx + 0.05, centery, centerz);
    // Green is y
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(centerx, centery, centerz);
    glVertex3f(centerx, centery + 0.05, centerz);
    // Blue is z
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(centerx, centery, centerz);
    glVertex3f(centerx, centery, centerz + 0.05);
    glEnd();
    
    // Render particle system
    ps->render();

    // Swap buffers
    glutSwapBuffers();

}

// Reshapes the display if the window is resized
void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(50, (GLfloat)w / (GLfloat)h, 0.1, 80.0);
    glMatrixMode(GL_MODELVIEW);
    width = w; 
    height = h;
}

// Callback function if the mouse moves
void mouse_motion(int x, int y) {

    // Calculate the lon and lat based on the mouse position
    mlon = ((GLfloat)x / (GLfloat)width) * -360.0 + 50.0;
    mlat = ((GLfloat)y / (GLfloat)height) * -100.0 + 50.0;
}

// Callback function if a button on the keyboard is pressed
void keyboard(unsigned char Key, int x, int y) {
    Pos wind;
    switch (Key) {
    case 'q':
        ps->setSkyY(ps->getSkyY() - 5);
        printf("SkyY: %f\n", ps->getSkyY());
        break;
    case 'w':
        ps->setSkyY(ps->getSkyY() + 5);
        printf("SkyY: %f\n", ps->getSkyY());
        break;
    case 'a':
        wind = ps->getWindVelocity();
        wind.x -= 0.2;
        ps->setWindVelocity(wind);
        printf("wind.x: %f\n", wind.x);
        printf("wind.z: %f\n", wind.z);
        break;
    case 's':
        wind = ps->getWindVelocity();
        wind.x += 0.2;
        ps->setWindVelocity(wind);
        printf("wind.x: %f\n", wind.x);
        printf("wind.z: %f\n", wind.z);
        break;
    case 'z':
        wind = ps->getWindVelocity();
        wind.z -= 0.2;
        ps->setWindVelocity(wind);
        printf("wind.x: %f\n", wind.x);
        printf("wind.z: %f\n", wind.z);
        break;
    case 'x':
        wind = ps->getWindVelocity();
        wind.z += 0.2;
        ps->setWindVelocity(wind);
        printf("wind.x: %f\n", wind.x);
        printf("wind.z: %f\n", wind.z);
        break;
    case 'e':
        ps->setRainLength(ps->getRainLength() - 0.2);
        printf("Rain length: %f\n", ps->getRainLength());
        break;
    case 'r':
        ps->setRainLength(ps->getRainLength() + 0.2);
        printf("Rain length: %f\n", ps->getRainLength());
        break;
    case 'd':
        ps->setNumParticles(ps->getNumParticles() - 500);
        printf("Number of particles: %d\n", ps->getNumParticles());
        //numLoops = 0;
        //totalElapsedTime = 0;
        break;
    case 'f':
        ps->setNumParticles(ps->getNumParticles() + 500);
        printf("Number of particles: %d\n", ps->getNumParticles());
        //numLoops = 0;
        //totalElapsedTime = 0;
        break;
    case 'c':
        ps->setTerminalVelocity(ps->getTerminalVelocity() + 0.1);
        printf("Average terminal velocity: %f\n", ps->getTerminalVelocity());
        break;
    case 'v':
        ps->setTerminalVelocity(ps->getTerminalVelocity() - 0.1);
        printf("Average terminal velocity: %f\n", ps->getTerminalVelocity());
        break;
    case 'j':
        ps->toggleRenderingSystem();
        printf("RENDERING SYSTEM TOGGLED\n");
        break;
    case ' ':
        ps->togglePause();
        break;
    default:
        break;
    }
}

// Initialise
void init() {
    glClearColor(0.55, 0.55, 0.6, 1.0);
    eyex = 0.0;  // Set eyepoint at eye height within the scene
    eyey = 2.0;
    eyez = 0.0;

    upx = 0.0;   // Set up direction to the +Y axis
    upy = 1.0;
    upz = 0.0;

    lat = 0.0;   // Look horizontally
    lon = 0.0;   // along the +Z axis

    // Initialise the particle system
    ps->init(3000, 0.55, 0.55, 0.6);

}

// Entry point of the program
int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize(width, height);
    glutCreateWindow("COMP27112 Lab Exercise 1");
    ShowCursor(false);
    
    init();

    // Register callback functions
    glutDisplayFunc(display);
    glutTimerFunc(1000, update, 0);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutPassiveMotionFunc(mouse_motion);

    // Start main loop
    glutMainLoop();
    return 0;
}